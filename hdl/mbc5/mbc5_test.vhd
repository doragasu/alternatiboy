--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   08:46:00 02/13/2015
-- Design Name:   
-- Module Name:   /home/jalon/src/github/mbc5/mbc5_test.vhd
-- Project Name:  mbc5
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: mbc5
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types STD_LOGIC and
-- STD_LOGIC_VECTOR for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.STD_LOGIC_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY mbc5_test IS
   CONSTANT t_clk : TIME := 1000 ns;
	CONSTANT t_bus : TIME := 150 ns;
	CONSTANT t_cs  : TIME := 100 ns;
	CONSTANT t_wr  : TIME := 200 ns;
	CONSTANT t_wr_rel : TIME := 350 ns;
	CONSTANT t_wr_rem : TIME := t_clk - t_bus - t_cs - t_wr - t_wr_rel;
END mbc5_test;
 
ARCHITECTURE mbc5_testbench OF mbc5_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT mbc5
    PORT(
         addr : IN  STD_LOGIC_VECTOR(15 DOWNTO 12);
         data : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
         n_wr : IN  STD_LOGIC;
         n_cs : IN  STD_LOGIC;
         n_rst : IN  STD_LOGIC;
         romb : OUT  STD_LOGIC_VECTOR(22 DOWNTO 14);
			led : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
			rum  : OUT STD_LOGIC;
         n_rcs : OUT  STD_LOGIC;
			n_oe : OUT STD_LOGIC
        );
    END COMPONENT;
    

   --Inputs
   SIGNAL addr : STD_LOGIC_VECTOR(15 DOWNTO 12) := (others => '0');
   SIGNAL data : STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');
   SIGNAL n_wr : STD_LOGIC := '0';
   SIGNAL n_cs : STD_LOGIC := '0';
   SIGNAL n_rst : STD_LOGIC := '0';

	--Outputs
   SIGNAL romb : STD_LOGIC_VECTOR(22 DOWNTO 14);
   SIGNAL n_rcs : STD_LOGIC;
	SIGNAL n_oe : STD_LOGIC;
	SIGNAL led : STD_LOGIC_VECTOR(2 DOWNTO 0);
	SIGNAL rum : STD_LOGIC;

   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
	-- Instantiate the Unit Under Test (UUT)
   uut: mbc5 PORT MAP (
          addr => addr,
          data => data,
          n_wr => n_wr,
          n_cs => n_cs,
          n_rst => n_rst,
          romb => romb,
          n_rcs => n_rcs,
			 n_oe => n_oe,
			 led => led,
			 rum => rum
        );
		PROCESS
		BEGIN
		-- -------------------- 0 us
		-- Undefined
		addr <= TRANSPORT "UUUU";
		data <= TRANSPORT "UUUUUUUU";
		n_wr <= TRANSPORT '1';
		n_cs <= TRANSPORT '1';
		n_rst <= TRANSPORT '0';
		-- -------------------- 1 us
		-- Reset for 1 us
		WAIT FOR t_clk;
		n_rst <= TRANSPORT '1';
		WAIT FOR t_clk;
		-- -------------------- 2 us
		-- Select ROM bank "100000010"
		WAIT FOR t_bus;
		addr <= TRANSPORT "0010"; -- rom_lb write
		data <= TRANSPORT "00000010";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem; -- 3 us, completed 1st cycle
		data <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		WAIT FOR t_bus;
		addr <= TRANSPORT "0011"; -- rom_hb write
		data <= "XXXXXXX1";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem; -- Completed 2nd cycle
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 4 us
		-- Read cycle (don't do anything)
		WAIT FOR t_bus;
		addr <= TRANSPORT "0010";
		WAIT FOR t_wr;
		data <= TRANSPORT "XXXXXXXX";
		WAIT FOR t_clk - t_bus - t_wr;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 5 us
		-- Write "010" to RGB LED
		WAIT FOR t_bus;
		addr <= TRANSPORT "0110"; -- rgbled write
		data <= TRANSPORT "00000010";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 6 us
		-- Enable rumble "1010"
		WAIT FOR t_bus;
		addr <= TRANSPORT "0111"; -- rumble write
		data <= TRANSPORT "00000001";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 7 us
		-- Select RAM bank "1010"
		WAIT FOR t_bus;
		addr <= TRANSPORT "0100"; -- ram_b write
		data <= TRANSPORT "XXXX1010";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 8 us
		-- Write to RAM, but RAM not enabled...
		WAIT FOR t_bus;
		addr <= TRANSPORT "1010"; -- RAM write
		data <= TRANSPORT "XXXXXXXX";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 9 us
		-- Enable RAM
		WAIT FOR t_bus;
		addr <= TRANSPORT "0000"; -- RAM enable
		data <= TRANSPORT "XXXX1010"; -- Magic nibble
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 10 us
		-- Write again to RAM. Now n_rcs should be generated
		WAIT FOR t_bus;
		addr <= TRANSPORT "1010"; -- RAM write
		data <= TRANSPORT "XXXXXXXX";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';		
		-- -------------------- 11 us
		-- Read from RAM, n_rcs should be generated
		WAIT FOR t_bus;
		addr <= TRANSPORT "1010";
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		data <= TRANSPORT "XXXXXXXX";
		WAIT FOR t_clk - t_bus - t_wr;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 12 us
		-- Disable RAM
		WAIT FOR t_bus;
		addr <= TRANSPORT "0000"; -- RAM disable
		data <= TRANSPORT "XXXX0XXX"; -- Magic nibble
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 13 us
		-- Write again to RAM, n_rcs should NOT be generated
		WAIT FOR t_bus;
		addr <= TRANSPORT "1010"; -- RAM write
		data <= TRANSPORT "XXXXXXXX";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';		
		-- -------------------- 14 us
		-- Read from RAM, n_rcs should NOT be generated
		WAIT FOR t_bus;
		addr <= TRANSPORT "1010";
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		data <= TRANSPORT "XXXXXXXX";
		WAIT FOR t_clk - t_bus - t_wr;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 15 us
		-- Disable rumble
		WAIT FOR t_bus;
		addr <= TRANSPORT "0111";
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		data <= TRANSPORT "XXXXXXX0";
		WAIT FOR t_clk - t_bus - t_wr;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 16 us
		-- Write "101" to LED register
		WAIT FOR t_bus;
		addr <= TRANSPORT "0110"; -- rgbled write
		data <= TRANSPORT "00000101";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem; -- 16 us, completed 1st cycle
		data <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 17 us
		-- Select ROM bank "010101010"
		WAIT FOR t_bus;
		addr <= TRANSPORT "0010"; -- rom_lb write
		data <= TRANSPORT "10101010";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem; -- 18 us, completed 1st cycle
		data <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		WAIT FOR t_bus;
		addr <= TRANSPORT "0011"; -- rom_hb write
		data <= "XXXXXXX0";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem; -- Completed 2nd cycle
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';		
		-- -------------------- 19 us
		-- Select RAM bank "0101"
		WAIT FOR t_bus;
		addr <= TRANSPORT "0100"; -- ram_b write
		data <= TRANSPORT "XXXX0101";
		WAIT FOR t_cs;
		n_cs <= TRANSPORT '0';
		WAIT FOR t_wr;
		n_wr <= TRANSPORT '0';
		WAIT FOR t_wr_rel;
		n_wr <= TRANSPORT '1';
		WAIT FOR t_wr_rem;
		addr <= TRANSPORT (OTHERS => 'Z');
		n_cs <= TRANSPORT '1';
		-- -------------------- 20 us
		-- Reset
		WAIT FOR t_bus;
		addr <= TRANSPORT (OTHERS => 'U');
		data <= TRANSPORT (OTHERS => 'U');
		n_rst <= TRANSPORT '0';
		WAIT;
	END PROCESS;
END mbc5_testbench;

CONFIGURATION mbc5_cfg OF mbc5_test IS
	FOR mbc5_testbench
	END FOR;
END mbc5_cfg;
