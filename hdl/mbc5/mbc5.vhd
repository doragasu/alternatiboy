-- mbc5.vhd: Gameboy MBC5 memory controller implementation
-- Jesus Alonso Fernandez (doragasu), 2015
-- Implementation based on information from:
--  + http://gbdev.gg8.se/wiki/articles/MBC5
--  + http://gbdev.gg8.se/wiki/articles/MBC1
-- License: GPLv3

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mbc5 IS
    PORT ( addr : IN  STD_LOGIC_VECTOR(15 DOWNTO 12); --A15 to A12
           data : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);   --D7 to D0
           n_wr : IN  STD_LOGIC;                      --Write Enable (neg)
           n_cs : IN  STD_LOGIC;                      --Chip Select (neg)
           n_rst: IN  STD_LOGIC;                      --Reset (neg)
           romb : OUT STD_LOGIC_VECTOR(22 DOWNTO 14); --ROM A22 to A14
                                                      --Also RAM A16 to A13
			  led  : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);	--RGB LED (active low)
			  rum  : OUT STD_LOGIC;                      --Rumble (active high)
           n_rcs: OUT STD_LOGIC;                      --RAM Chip Select (neg)
			  n_oe : OUT STD_LOGIC);                     --Transceiver OE (neg)
END mbc5;

ARCHITECTURE behavioral OF mbc5 IS
	-- RAM Enable
	SIGNAL ram_enable : STD_LOGIC;
	-- ROM bank, default value selects bank 1. This is the expected MBC1
	-- behaviour, but I don't know if it is also correct for MBC5.
	SIGNAL rom_bank   : STD_LOGIC_VECTOR(8 DOWNTO 0);
	-- RAM bank
	SIGNAL ram_bank   : STD_LOGIC_VECTOR(3 DOWNTO 0);
	-- Selected register: none, RAM enable, ROM bank (lower 8 bits), ROM bank
	-- (higher bit), RAM bank.
	TYPE reg IS (none, ram_en, rom_lb, rom_hb, ram_b, rgbled, rumble);
	SIGNAL reg_sel    : reg;
	-- SRAM chip select.
	SIGNAL rcs        : STD_LOGIC;
	-- RGB LED register
	SIGNAL led_reg    : STD_LOGIC_VECTOR(2 DOWNTO 0);
	-- Temporal signal containing RAM bank or lower 4 bits of ROM bank
	SIGNAL ram_rom_l	: STD_LOGIC_VECTOR(3 DOWNTO 0);
	-- Rumble register
	SIGNAL rum_reg : STD_LOGIC;
BEGIN
	-- Check if any register is selected
	WITH addr SELECT
		reg_sel <= ram_en WHEN "0000" | "0001",
				   rom_lb WHEN "0010",
				   rom_hb WHEN "0011",
				   ram_b  WHEN "0100" | "0101",
					rgbled WHEN "0110",
					rumble WHEN "0111",
				   none WHEN OTHERS;

	-- Update register value, if reset or selected
	PROCESS(n_rst, n_cs, n_wr)
	BEGIN
		IF n_rst = '0' THEN
			-- Reset, write default register values
			ram_enable <= '0';
			rom_bank <= "000000001"; -- WARNING: Correct for MBC5?
			ram_bank <= "0000";
			rum_reg <= '0';
			led_reg <= "111"; -- LED off
		ELSIF n_wr = '0' THEN
			-- Cartridge write, check if we have to update any register
			IF reg_sel = ram_en THEN
				-- RAM enable, check magic nibble
				IF data(3 DOWNTO 0) = "1010" THEN
					ram_enable <= '1';
				ELSE
					ram_enable <= '0';
				END IF;
--				led_reg <= "110";		-- DEBUG
			ELSIF reg_sel = rom_lb THEN
				-- ROM bank lower byte select
				rom_bank(7 DOWNTO 0) <= data;
--				led_reg <= "101";		-- DEBUG
			ELSIF reg_sel = rom_hb THEN
				-- ROM bank upper bit select
				rom_bank(8) <= data(0);
--				led_reg <= "100";		-- DEBUG
			ELSIF reg_sel = ram_b THEN
				-- RAM bank select
				ram_bank <= data(3 DOWNTO 0);
--				led_reg <= "101";		-- DEBUG
			ELSIF reg_sel = rgbled THEN
				-- Set RGB LED color
				led_reg <= data(2 DOWNTO 0);
--				led_reg <= "110";		-- DEBUG
			ELSIF reg_sel = rumble THEN
				-- Enable/disable rumble
				rum_reg <= data(0);
			END IF;
		END IF;
	END PROCESS;

	-- Update outputs based on register values
	-- RAM CS active only when ram_enable and address in range A000~BFFF
	rcs <= ram_enable AND NOT n_cs AND addr(15) AND NOT addr(14) AND
			 addr(13);

	-- When addressing 0000 ~ 3FFF, select bank 0
	WITH addr(15 DOWNTO 14) SELECT
		romb(22 DOWNTO 18) <= "00000" WHEN "00",
									  rom_bank(8 DOWNTO 4) WHEN OTHERS;

	-- When addressing RAM, output RAM bank, else output ROM bank
	ram_rom_l <= ram_bank WHEN rcs = '1' ELSE rom_bank(3 DOWNTO 0);
	WITH addr(15 DOWNTO 14) SELECT
		romb(17 DOWNTO 14) <= "0000" WHEN "00",
									 ram_rom_l WHEN OTHERS;

	n_rcs <= NOT(rcs);
	-- Enable transceiver output when accessing ROM space or
	-- reading/writing to SRAM
	n_oe <= NOT(NOT addr(15) OR (NOT n_cs AND rcs));
	-- Update RGB LED status
	led(2 DOWNTO 0) <= led_reg(2 DOWNTO 0);
	-- Update rumble status
	rum <= rum_reg;
END behavioral;

